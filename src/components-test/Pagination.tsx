import { useState } from "react";
import Link from "next/link";

type Props = {
  totalPages: number;
  currentPage: number;
};

const Pagination = ({ totalPages, currentPage }: Props) => {
  const [pagesToShow] = useState(5);

  const startPage = Math.max(1, currentPage - Math.floor(pagesToShow / 2));
  const endPage = Math.min(totalPages, startPage + pagesToShow - 1);

  return (
    <div className="flex justify-center">
      {startPage > 1 && (
        <Link href={`?page=${startPage - 1}`}>
          <span className="pagination-item">{"<<"}</span>
        </Link>
      )}
      {Array.from({ length: endPage - startPage + 1 }, (_, i) => i + startPage).map(
        (page) => (
          <Link key={page} href={`?page=${page}`}>
            <span
              className={`pagination-item ${
                currentPage === page ? "active" : ""
              }`}
            >
              {page}
            </span>
          </Link>
        )
      )}
      {endPage < totalPages && (
        <Link href={`?page=${endPage + 1}`}>
          <span className="pagination-item">{">>"}</span>
        </Link>
      )}
    </div>
  );
};

export default Pagination;
