import React from 'react'

type oscarProps = {
  children: React.ReactNode
}

export const Oscars = (props: oscarProps) => {
  return (
    <div>
        <h2>{props.children}</h2>
    </div>
  )
}
