import React from 'react'

type inputPropos = {
    value: string,
    handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void,
}

export const Input = ({value, handleChange}: inputPropos) => {

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => { console.log(event)}

    return (
        <div className='my-10'>
            <input 
            type='text' 
            value={value} 
            onChange={handleChange}
            className='px-4 py-2 border border-slate-300 focus:outline-none rounded-md' placeholder='Placeholder text' />
        </div>
    )
}
