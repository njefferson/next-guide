import React from 'react'

type buttonProps = {
    handleClick: (event: React.MouseEvent<HTMLButtonElement>, id: number) => void
}

export const Button: React.FC<buttonProps> = ({handleClick}) => {
  return (
    <button
    onClick={(event) => handleClick(event, 1)}
    type='button'
    className={` py-[14px] px-[40px] text-white text-[14px] rounded-[14px] bg-primary_color shadow-button`}
    >
        <div className="flex items-center gap-4">
            {"Click"}
        </div>
    </button>
  )
}
