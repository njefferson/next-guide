import React from 'react'

type personProps = {
    name: {
        firstname: string,
        lastname: string,
    };

}

export const Person = (props: personProps) => {
  return (
    <div className='mb-10'>
        <h2 className='text-[16px] text-color-black mb-4'>{"Individuel"}</h2>
        <div className='bg-blue-100 rounded-full px-6 py-1 inline-flex'>
            <h2 className='text-[16px] text-blue-500 flex items-center gap-2'>{props.name.lastname}<span className='font-[600]'>{props.name.firstname}</span></h2>
        </div>
    </div>
  )
}


// ------------------- OU --------------------

/*
    import React from 'react'

    type personProps = {
        name: {
            firstname: string,
            lastname: string,
        };

    }

    export const Person: React.FC<personProps> = ({ name }) => {
    return (
        <div>{name.firstname} {name.lastname}</div>
    )
    }

*/
