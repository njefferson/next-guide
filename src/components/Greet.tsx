import React from 'react'

type geetProps = {
    name: string;
    messageCount: number;
    isLoggedIn: boolean;
}

export const Greet: React.FC<geetProps> = ({ name, messageCount, isLoggedIn }) => {
  return (
    <div className='mb-10 py-8 border-b border-slate-200'>
        <div className='px-6 py-2 inline-block bg-violet-100 rounded-md'>
            <h2 className='text-[16px] font-[500] text-violet-600'>
                {
                    isLoggedIn ?
                    `Welcome ${name}! you have ${messageCount} unread messages`
                    :
                    `Welcome Guest`
                }
            </h2>
        </div>
    </div>
  )
}

/*
    const Greet: React.FC<geetProps> = ({ name })
    ------ ou -----
    const Greet = (props: geetProps)
*/
