import React from 'react'

type statusProps = {
    /* type UNION de typescript */
    status: 'loading' | 'success' | 'error'; 
}

export const Status: React.FC<statusProps> = ({status}) => {

    let message;

    if(status === "loading"){
        message = "Loading ..."
    }else if(status === "success"){
        message = "Data fetched successfully !"
    }else if(status === "error"){
        message = "Error fetching data"
    }


    return (
        <div>
            <h2>Status - {message}</h2>
        </div>
    )
}
