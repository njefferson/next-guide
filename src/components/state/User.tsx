import { useState } from "react";

type AuthUser = {
    name: string
    email: string
}

export const User = () => {
    const [user, setUser] = useState<AuthUser>({} as AuthUser)
    const handleLogin = () => {
        setUser(
            {
                name: "jeff",
                email: "jeffersonelidje@gmail.com",
            }
        )
    }
    // const handleLogout = () => {
    //     setUser(null)
    // } 

    return (
        <div className="flex items-center gap-4">
            <button
            onClick={handleLogin}
            type='button'
            className={` py-[14px] px-[40px] text-white text-[14px] rounded-[14px] bg-primary_color shadow-button`}
            >
                <div className="flex items-center gap-4">
                    {"Login"}
                </div>
            </button>
            {/* <button
            onClick={handleLogout}
            type='button'
            className={` py-[14px] px-[40px] text-white text-[14px] rounded-[14px] bg-primary_color shadow-button`}
            >
                <div className="flex items-center gap-4">
                    {"Logout"}
                </div>
            </button> */}
           <div>
                <div>User name is {user.name}</div>
                <div>User email is {user.email}</div>
           </div>
        </div>
    )
}