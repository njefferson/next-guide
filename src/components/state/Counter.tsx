import { useReducer } from "react";

type CounterState = {
    count: number
}

type UpdateAction = {
    type: "increment" | "decrement"
    payload: number
}

type ResetAction = {
    type: "reset"
}

type CounterAction = UpdateAction | ResetAction

const initialState = { count: 0}

function reducer(state: CounterState, action: CounterAction) {
    switch(action.type) {
        case 'increment':
            return { count: state.count + action.payload}
        case 'decrement':
            return { count: state.count - action.payload  }
        case 'reset':
            return initialState
        default:
            return state
    }
}

export const Counter = () => {
    const [state, dispatch] = useReducer(reducer, initialState)
    return(
        <div className="flex items-center gap-4">
            <button
            onClick={() => dispatch({ type: 'increment', payload: 10})}
            type='button'
            className={` py-[14px] px-[40px] text-white text-[14px] rounded-[14px] bg-primary_color shadow-button`}
            >
                <div className="flex items-center gap-4">
                    {"Inscrement 10"}
                </div>
            </button>
            Count: {state.count}
            <button
            onClick={() => dispatch({ type: 'decrement', payload: 10})}
            type='button'
            className={` py-[14px] px-[40px] text-white text-[14px] rounded-[14px] bg-primary_color shadow-button`}
            >
                <div className="flex items-center gap-4">
                    {"Decrement 10"}
                </div>
            </button>
            <button
            onClick={() => dispatch({ type: 'reset'})}
            type='button'
            className={` py-[14px] px-[40px] text-white text-[14px] rounded-[14px] bg-primary_color shadow-button`}
            >
                <div className="flex items-center gap-4">
                    {"Reset"}
                </div>
            </button>
        </div>
    )
}