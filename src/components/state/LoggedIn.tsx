import { useState } from "react"

export const LoggedIn = () => {

    const [isLoggedIn, setIsLoggedIn] = useState(false)

    const handleLogin = () => {
        setIsLoggedIn(true)
    }
    const handleLogout = () => {
        setIsLoggedIn(false)
    }

    return (
        <div>
            <button
            onClick={handleLogin}
            type='button'
            className={` py-[14px] px-[40px] text-white text-[14px] rounded-[14px] bg-primary_color shadow-button`}
            >
                <div className="flex items-center gap-4">
                    {"Login"}
                </div>
            </button>
            <button
            onClick={handleLogout}
            type='button'
            className={` py-[14px] px-[40px] text-white text-[14px] rounded-[14px] bg-primary_color shadow-button`}
            >
                <div className="flex items-center gap-4">
                    {"Logout"}
                </div>
            </button>
            <div>User is  { isLoggedIn ? 'logged in' : 'logged out' }</div>
        </div>
    )
}