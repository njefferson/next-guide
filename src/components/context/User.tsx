export const User = () => {
    const handleLogin = () => {}
    const handleLogout = () => {}

    return (
        <div className="flex items-center gap-4">
            <button
            onClick={handleLogin}
            type='button'
            className={` py-[14px] px-[40px] text-white text-[14px] rounded-[14px] bg-primary_color shadow-button`}
            >
                <div className="flex items-center gap-4">
                    {"Login"}
                </div>
            </button>
            <button
            onClick={handleLogout}
            type='button'
            className={` py-[14px] px-[40px] text-white text-[14px] rounded-[14px] bg-primary_color shadow-button`}
            >
                <div className="flex items-center gap-4">
                    {"Logout"}
                </div>
            </button>
            <div>User name is</div>
            <div>User email is</div>
        </div>
    )
}