import React from 'react'

type personsListProps = {
    names: {
        firstname: string,
        lastname: string,
    }[]
}

export const PersonList: React.FC<personsListProps> = ({names})  => {
  return (
    <div className='mb-10'>
        <h2 className='text-[16px] text-color-black mb-4'>{"Groupe"}</h2>
        <div className='flex items-center gap-6'>
            {
                names.map(name => {
                    return (
                        <div key={name.firstname} className='bg-amber-50 rounded-full px-6 py-1 inline-flex'>
                            <h2 className='text-[16px] text-amber-500 flex items-center gap-2'>
                                {name.lastname} 
                                <span className='font-[600]'>{name.firstname}</span>
                            </h2>
                        </div>
                    )
                })
            }
        </div>
    </div>
  )
}
