/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        body: ['Euclid Circular A'],
        title: ['Fellix'],
      },
      boxShadow: {
        'base': "0 0.3rem 0.94rem rgba(128,128,128,0.06)",
        'sh-me': '0 0.3rem 0.94rem #8080800f',
        'sh-xs': '0px 1px 2px 0px #1018280D',
        'sh-sm': '0px 1px 2px 0px #1018280F, 0px 1px 3px 0px #1018281A',
        'sh-md': '0px 2px 4px -2px #1018280F, 0px 4px 8px -2px #1018281A',
        'sh-lg': '0px 12px 16px -4px rgba(16, 24, 40, 0.08), 0px 4px 6px -2px rgba(16, 24, 40, 0.03)'
      },
      colors: {
        'color-black': "#35383B",

        'primary_color' : "#217BF4",
        'primary_color-300': "#5CA1FE",
        'primary_color-100': "#E5F0FD",

        'secondary_color' : "#0A093D",

        'color_gray': "#656464",

        'orange_color': "#FFAF2E",
        'orange_color-300': "#FFD085",

        'red_color': "#F04148",
        'red_color-300': "#FF858A",



        'primary-50': "#FFE2DC",
        'primary-100': "#ECC0BD",
        'primary-200': "#CD9C9A",
        'primary-300': "#AF7775",
        'primary-400': "#985B5A",
        'primary-500': "#824040",
        'primary-600': "#76383A",
        'primary-700': "#652D30",
        'primary-800': "#552128",

        'secondary-50': "#FCEDEE",
        'secondary-100': "#F9D1D3",
        'secondary-200': "#E5A29E",
        'secondary-300': "#D8817A",
        'secondary-400': "#E06859",
        'secondary-500': "#E35E43",
        'secondary-600': "#D55641",
        'secondary-700': "#C34D3B",
        'secondary-800': "#B64735",

        'tertiary-50': "#FCE4DC",
        'tertiary-100': "#F9C39F",
        'tertiary-200': "#ED9E65",
        'tertiary-300': "#DF7C2A",
        'tertiary-400': "#D56500",
        'tertiary-500': "#CB5000",
        'tertiary-600': "#C24A00",
        'tertiary-700': "#B64300",
        'tertiary-800': "#AA3C00",

        'neutral-c1-100': "#E6E3F4",
        'neutral-c1-200': "#CEC9EA",
        'neutral-c1-300': "#9D97C0",
        'neutral-c1-400': "#646082",
        'neutral-c1-500': "#211F30",
        'neutral-c1-600': "#181629",
        'neutral-c1-700': "#110F22",
        'neutral-c1-800': "#0B091B",
        'neutral-c1-900': "#070517",

        'neutral-c2-100': "#EDF2F8",
        'neutral-c2-200': "#DCE6F1",
        'neutral-c2-300': "#B9C5D5",
        'neutral-c2-400': "#8F9AAB",
        'neutral-c2-500': "#5B6474",
        'neutral-c2-600': "#424D63",
        'neutral-c2-700': "#2D3853",
        'neutral-c2-800': "#1D2643",
        'neutral-c2-900': "#111937",
      },
      boxShadow: {
        'base': '0 0.3rem 0.94rem rgba(128,128,128,0.06)',
        'button' : '0px 7px 22px -6px rgba(33, 123, 244, 0.34)',
      }
    },
    screens: {
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }
      
    },
  },
  plugins: [],
}