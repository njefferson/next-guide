import { Fragment, useState, useEffect } from 'react'
import { useRouter } from "next/router";
import { GetServerSideProps } from "next";
import axios from "axios";
import Link from "next/link";
import { Listbox, Transition } from '@headlessui/react'
import { BsCheck } from "react-icons/bs"
import { FiChevronDown } from "react-icons/fi"
import { Pagination } from '@/src/components-test';

type Option = {
    label: string;
    value: string;
};

type Post = {
    id: number,
    title: string;
    body: string
}

interface Props {
  posts: Post[];
  totalCount: number;
}

const PAGE_SIZE = 8;

const options: Option[] = [
    { label: "Tous les posts", value: "" },
    { label: "Post 1", value: "1" },
    { label: "Post 2", value: "2" },
    { label: "Post 3", value: "3" },
    { label: "Post 4", value: "4" },
    { label: "Post 5", value: "5" },
    { label: "Post 6", value: "6" },
    { label: "Post 7", value: "7" },
    { label: "Post 8", value: "8" },
    { label: "Post 9", value: "9" },
    { label: "Post 10", value: "10" },
]

export default function Home({ posts, totalCount }: Props) {
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState<Post[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const router = useRouter();
  const [selectedOption, setSelectedOption] = useState<Option>(options[0]);

    // search
    useEffect(() => {
        if (searchTerm === "") {
            setSearchResults(posts);
            setTotalPages(Math.ceil(totalCount / PAGE_SIZE));
            return;
        }

        const filteredPosts = posts.filter((post) =>
        post.title.toLowerCase().includes(searchTerm.toLowerCase())
        );

        setSearchResults(filteredPosts);
        setTotalPages(Math.ceil(filteredPosts.length / PAGE_SIZE));
        setCurrentPage(1);
    }, [searchTerm, posts, totalCount]);

    const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchTerm(event.target.value);
    };


    //   pagination
    const numPages = Math.ceil(posts?.length / PAGE_SIZE);
    const pages = Array.from({ length: numPages }, (_, i) => i + 1);

    // const paginatedPosts = posts?.slice(
    //     (currentPage - 1) * PAGE_SIZE,
    //     currentPage * PAGE_SIZE
    // );

    const handlePageChange = async (page: number) => {
        setCurrentPage(page);
        await router.push("/test_5")
    };

    // filter
    const handleOptionSelect = async (option: Option) => {
        setSelectedOption(option);
        if (option.value === "") {
          // Récupérer tous les posts
          const { data } = await axios.get(
            "https://jsonplaceholder.typicode.com/posts"
          );
          setSearchResults(data);
        } else {
          // Récupérer les posts qui correspondent à l'option sélectionnée
          const { data } = await axios.get(
            `https://jsonplaceholder.typicode.com/posts?userId=${option.value}`
          );
          setSearchResults(data);
        }
      };

  return (
    <div className="base-container mx-auto py-8">
        <h1 className="text-3xl font-bold mb-4 text-center">NextJs - Typescript - TailwindCSS</h1>
        <div className='flex items-center justify-between mb-8 py-10'>
            <div className="w-1/2">
                <input
                type="text"
                className="w-full h-[49px] shadow-sh-xs transition-all placeholder:font-[400] placeholder:text-c-neutral-500 border-[1px] border-neutral-c2-100 focus:border-neutral-c2-200 focus:ring-4 focus:ring-neutral-c2-200 hover:ring-4 hover:ring-neutral-c2-200 text-[16px] leading-[24px] text-neutral-c2-900 focus:outline-none px-[10px] py-[12px] rounded-[4px]"
                placeholder="Search posts..."
                value={searchTerm}
                onChange={handleSearchChange}
                />
            </div>
            <div className="w-72">
                <Listbox value={selectedOption} onChange={handleOptionSelect}>
                    <div className="relative mt-1">
                    <Listbox.Button className="relative w-full h-[49px] shadow-sh-xs cursor-pointer rounded-md shadow-base border border-neutral-c2-100 bg-white py-2 pl-3 pr-10 text-left focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm">
                        <span className="block truncate">{selectedOption.label}</span>
                        <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                        <FiChevronDown
                            className="h-5 w-5 text-gray-400"
                            aria-hidden="true"
                        />
                        </span>
                    </Listbox.Button>
                    <Transition
                        as={Fragment}
                        leave="transition ease-in duration-100"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <Listbox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                        {options.map((option, index) => (
                            <Listbox.Option
                            key={index} value={option}
                            className={({ active }) =>
                                `relative cursor-pointer select-none py-2 pl-10 pr-4 ${
                                active ? 'bg-neutral-c2-100 text-neutral-c2-600' : 'text-gray-900'
                                }`
                            }
                            >
                            {({ selected }) => (
                                <>
                                <span
                                    className={`block truncate ${
                                    selected ? 'font-medium' : 'font-normal'
                                    }`}
                                >
                                    {option.label}
                                </span>
                                {selected ? (
                                    <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-secondary-500">
                                        <BsCheck className="h-5 w-5" aria-hidden="true" />
                                    </span>
                                ) : null}
                                </>
                            )}
                            </Listbox.Option>
                        ))}
                        </Listbox.Options>
                    </Transition>
                    </div>
                </Listbox>
            </div>
        </div>
        <div className="grid grid-cols-4 gap-6">
            {searchResults.slice((currentPage - 1) * PAGE_SIZE, currentPage * PAGE_SIZE).map((post) => (
                <div key={post.id} className="bg-white rounded-md shadow-base p-8 border border-neutral-100">
                    <Link href={`/posts/${post.id}`}>
                        <h2 className="text-xl font-semibold mb-4 --limit-text-2">{post.title}</h2>
                    </Link>
                    <p className="text-gray-500 mb-4 --limit-text-3">{post.body}</p>
                </div>
            ))}
        </div>
        {searchResults.length === 0 && (
            <p className="text-red-500 text-center">Aucun résulat pour "{searchTerm}"</p>
        )}
        {
            searchResults.length !== 0 && (
                <div className="flex justify-center items-center my-8">
                    {currentPage > 1 && (
                        <button
                        className="mx-2 px-2 py-1 rounded bg-white text-gray-500 hover:bg-neutral-c2-100"
                        onClick={() => handlePageChange(currentPage - 1)}
                        >
                        Prev
                        </button>
                    )}
                    {pages.map((page, index) => {
                        if (
                        (currentPage <= 3 && index < 4) ||
                        (currentPage > 3 && index === 0) ||
                        (currentPage > 3 && index === numPages - 1) ||
                        (currentPage > 3 && index >= currentPage - 2 && index <= currentPage)
                        ) {
                        return (
                            <button
                            key={page}
                            className={`px-3 py-1 mr-2 rounded ${
                                page === currentPage ? "bg-secondary-500 text-white" : "bg-neutral-c2-100"
                            }`}
                            onClick={() => handlePageChange(page)}
                            >
                            {page}
                            </button>
                        );
                        } else if (
                        (currentPage <= 3 && index === 3) ||
                        (currentPage > 3 && index === 1) ||
                        (currentPage > 3 && index === currentPage - 2)
                        ) {
                        return <span key={page} className='mr-1'>...</span>;
                        } else {
                        return null;
                        }
                    })}
                    {currentPage < numPages && (
                        <button
                        className="mx-1 px-2 py-1 rounded bg-white text-gray-500 hover:bg-neutral-c2-100"
                        onClick={() => handlePageChange(currentPage + 1)}
                        >
                        Next
                        </button>
                    )}
                </div>
            )
        }
        {/* <Pagination
            currentPage={currentPage}
            totalPages={totalPages}
            // onPageChange={handlePageChange}
        /> */}
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  
    const { data, headers } = await axios.get(
      "https://jsonplaceholder.typicode.com/posts",
    );
  
    const totalCount = parseInt(headers["x-total-count"]);
  
    return {
      props: {
        posts: data,
        totalCount,
      },
    };
  };