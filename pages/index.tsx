import { Button, Greet, Heading, Person, PersonList, Status } from '@/src/components'
import { Oscars } from '@/src/components/Oscars'
import React from 'react'
import { Input } from '../src/components/Input';
import { User } from '@/src/components/state/User';
import { Counter } from '@/src/components/state/Counter';
import { ThemeContextProvider } from '@/src/components/context/ThemeContext';
import { Box } from '@/src/components/context/Box';

export default function Home() {
  
  const personName = {
      firstname: "Jean",
      lastname: "Yves"
  }

  const nameList = [
      {
        firstname: "Bruce",
        lastname: "Wayne"
      },
      {
        firstname: "Clark",
        lastname: "Kent"
      },
      {
        firstname: "Princesse",
        lastname: "Diana"
      }
  ]

  return (
    <div className='base-container'>
      <Greet name="Nick Jefferson" messageCount={20} isLoggedIn={false} />
      <Status status="success" />
      <div className='my-16'>
          <Person name={personName} />
          <PersonList names={nameList} />
      </div>
      <Heading>Placeholder text</Heading>
      <Oscars>
          <Heading>Oscar goes to Leonardo Dicaprio !</Heading>
      </Oscars>
      <div className='my-16'></div>
      <Button handleClick={(event, id) => {
        console.log("Yes man !!!", event, id)
      }} />
      <Input value=""  handleChange={(event) => console.log("Input")} />
      <div className='my-16'></div>
      <User />
      <div className='my-16'></div>
      <Counter />
      <div className='my-16'></div>
      <ThemeContextProvider>
        <Box />
      </ThemeContextProvider>
      <div className='my-16'></div>
    </div>
  )
}
