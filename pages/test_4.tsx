import { useState } from "react";
import axios from "axios";
import { GetServerSideProps } from "next";
import { useRouter } from "next/router";
// import { User } from "../types";

export type User = {
    id: number;
    name: string;
    email: string;
}

interface Props {
  users: User[];
}

const PAGE_SIZE = 4;

export default function Home({ users }: Props) {
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState<User[]>([]);
  const [errorMessage, setErrorMessage] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const router = useRouter();

  const handleSearch = async () => {
    const response = await axios.get(
      `https://jsonplaceholder.typicode.com/users?q=${searchTerm}`
    );

    const results = response.data as User[];

    if (results.length === 0) {
      setErrorMessage("No results found.");
    } else {
      setErrorMessage("");
      setSearchResults(results);
    }
  };

  const debounce = (func: () => void, delay: number) => {
    let timer: NodeJS.Timeout;
    return (..._args: any[]) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        func();
      }, delay);
    };
  };

  const handleSearchDebounce = debounce(handleSearch, 500);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
    handleSearchDebounce();
  };
  

    // pagination
    const numPages = Math.ceil(users.length / PAGE_SIZE);
    const pages = Array.from({ length: numPages }, (_, i) => i + 1);
  
    const handlePageChange = async (page: number) => {
      setCurrentPage(page);
      await router.push("/test_4");
    };
  
    const paginatedUsers = users.slice(
      (currentPage - 1) * PAGE_SIZE,
      currentPage * PAGE_SIZE
    );

  return (
    <div className="max-w-2xl mx-auto px-4 py-8">
        <div className="mb-8">
                <label htmlFor="searchTerm" className="sr-only">
                    Search
                </label>
                <input
                    type="text"
                    id="searchTerm"
                    className="border border-gray-400 px-4 py-2 w-full rounded"
                    placeholder="Search posts..."
                    value={searchTerm}
                    onChange={handleInputChange}
                />
        </div>


        {errorMessage ?
            <p className="text-red-500 my-2">{errorMessage}</p>
            :
            <>
            {searchResults.length > 0 ? (
                <ul>
                {searchResults.map((users) => (
                    <li key={users.id} className="mb-4">
                    <h2 className="text-xl font-bold">{users.name}</h2>
                    <p className="text-gray-700">{users.email}</p>
                    </li>
                ))}
                </ul>
            ) : (
                <ul>
                {paginatedUsers.map((users) => (
                    <li key={users.id} className="mb-4">
                    <h2 className="text-xl font-bold">{users.name}</h2>
                    <p className="text-gray-700">{users.email}</p>
                    </li>
                ))}
                </ul>
            )}

                <div className="flex justify-center mt-8">
                    {pages.map((page) => (
                    <button
                        key={page}
                        className={`px-3 py-1 mr-2 rounded ${
                        page === currentPage ? "bg-blue-500 text-white" : "bg-gray-200"
                        }`}
                        onClick={() => handlePageChange(page)}
                    >
                        {page}
                    </button>
                    ))}
                </div>
            </>
        }

    </div>
  );
}

export const getServerSideProps = async () => {
  const response = await axios.get(
    "https://jsonplaceholder.typicode.com/users"
  );

  const users = response.data as User[];

  return {
    props: {
      users,
    },
  };
};
