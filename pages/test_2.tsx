import { useState } from "react";
import axios from "axios";
import Link from "next/link";
import { GetServerSideProps } from "next";
import { useRouter } from "next/router";
// import { Post } from "../types";

type Post = {
    id: number,
    title: string,
}

interface Props {
  posts: Post[];
}

const PAGE_SIZE = 10;

export default function Home({ posts }: Props) {
  const [currentPage, setCurrentPage] = useState(1);
  const router = useRouter();

  const numPages = Math.ceil(posts.length / PAGE_SIZE);
  const pages = Array.from({ length: numPages }, (_, i) => i + 1);

  const handlePageChange = async (page: number) => {
    setCurrentPage(page);
    await router.push("/test_2");
  };

  const paginatedPosts = posts.slice(
    (currentPage - 1) * PAGE_SIZE,
    currentPage * PAGE_SIZE
  );

  return (
    <div className="max-w-2xl mx-auto px-4 py-8">
      <h1 className="text-2xl font-bold mb-4">Latest Posts</h1>
      <ul>
        {paginatedPosts.map((post) => (
          <li key={post.id} className="mb-4">
            <Link href={`/posts/${post.id}`}>
              <span className="text-blue-500 hover:underline">{post.title}</span>
            </Link>
          </li>
        ))}
      </ul>
      <div className="flex justify-center my-8">
            {currentPage > 1 && (
                <button
                className="mx-1 px-2 py-1 rounded-lg bg-white text-gray-500 hover:bg-gray-200"
                onClick={() => handlePageChange(currentPage - 1)}
                >
                Prev
                </button>
            )}
            {pages.map((page, index) => {
                if (
                (currentPage <= 3 && index < 4) ||
                (currentPage > 3 && index === 0) ||
                (currentPage > 3 && index === numPages - 1) ||
                (currentPage > 3 && index >= currentPage - 2 && index <= currentPage)
                ) {
                return (
                    <button
                    key={page}
                    className={`px-3 py-1 mr-2 rounded ${
                        page === currentPage ? "bg-blue-500 text-white" : "bg-gray-200"
                    }`}
                    onClick={() => handlePageChange(page)}
                    >
                    {page}
                    </button>
                );
                } else if (
                (currentPage <= 3 && index === 3) ||
                (currentPage > 3 && index === 1) ||
                (currentPage > 3 && index === currentPage - 2)
                ) {
                return <span key={page} className='mr-1'>...</span>;
                } else {
                return null;
                }
            })}
            {currentPage < numPages && (
                <button
                className="mx-1 px-2 py-1 rounded-lg bg-white text-gray-500 hover:bg-gray-200"
                onClick={() => handlePageChange(currentPage + 1)}
                >
                Next
                </button>
            )}
        </div>
      {/* <div className="flex justify-center mt-8">
        {pages.map((page) => (
          <button
            key={page}
            className={`px-3 py-1 mr-2 rounded ${
              page === currentPage ? "bg-blue-500 text-white" : "bg-gray-200"
            }`}
            onClick={() => handlePageChange(page)}
          >
            {page}
          </button>
        ))}
      </div> */}
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  const response = await axios.get(
    "https://jsonplaceholder.typicode.com/posts"
  );

  const posts = response.data as Post[];

  return {
    props: {
      posts,
    },
  };
};