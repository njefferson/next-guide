import { useState } from "react";
import axios from "axios";
// import { User } from "../types";

export type User = {
    id: number;
    name: string;
    email: string;
}

interface Props {
  users: User[];
}


export default function Home({ users }: Props) {
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState<User[]>([]);
  const [errorMessage, setErrorMessage] = useState("");

  const handleSearch = async () => {
    const response = await axios.get(
      `https://jsonplaceholder.typicode.com/users?q=${searchTerm}`
    );

    const results = response.data as User[];

    if (results.length === 0) {
      setErrorMessage("No results found.");
    } else {
      setErrorMessage("");
      setSearchResults(results);
    }
  };

  const debounce = (func: () => void, delay: number) => {
    let timer: NodeJS.Timeout;
    return (..._args: any[]) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        func();
        // func(...args);
      }, delay);
    };
  };

  const handleSearchDebounce = debounce(handleSearch, 500);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
    handleSearchDebounce();
  };

  return (
    <div className="max-w-2xl mx-auto px-4 py-8">
        <div className="mb-8">
                <label htmlFor="searchTerm" className="sr-only">
                    Search
                </label>
                <input
                    type="text"
                    id="searchTerm"
                    className="border border-gray-400 px-4 py-2 w-full rounded"
                    placeholder="Search posts..."
                    value={searchTerm}
                    onChange={handleInputChange}
                />
        </div>


        {errorMessage ? 
            <p className="text-red-500 my-2">{errorMessage}</p>
            :
            <>
            {searchResults.length > 0 ? (
                <ul>
                {searchResults.map((users) => (
                    <li key={users.id} className="mb-4">
                    <h2 className="text-xl font-bold">{users.name}</h2>
                    <p className="text-gray-700">{users.email}</p>
                    </li>
                ))}
                </ul>
            ) : (
                <ul>
                {users.map((users) => (
                    <li key={users.id} className="mb-4">
                    <h2 className="text-xl font-bold">{users.name}</h2>
                    <p className="text-gray-700">{users.email}</p>
                    </li>
                ))}
                </ul>
            )}
            </>
        }

    </div>
  );
}

export const getServerSideProps = async () => {
  const response = await axios.get(
    "https://jsonplaceholder.typicode.com/users"
  );

  const users = response.data as User[];

  return {
    props: {
      users,
    },
  };
};
