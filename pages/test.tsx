import { useState } from "react";
import axios from "axios";
import Link from "next/link";
import { GetServerSideProps } from "next";
import { useRouter } from "next/router";
// import { Post } from "../types";

type Post = {
    id: number,
    title: string,
}

interface Props {
  posts: Post[];
  totalPosts: number;
}

const PAGE_SIZE = 10;

export default function Home({ posts, totalPosts }: Props) {
  const [currentPage, setCurrentPage] = useState(1);
  const router = useRouter();

  const numPages = Math.ceil(totalPosts / PAGE_SIZE);
  const pages = Array.from({ length: numPages }, (_, i) => i + 1);

  const handlePageChange = async (page: number) => {
    setCurrentPage(page);
    await router.push(`/test/?page=${page}`);
  };

  return (
    <div className="max-w-2xl mx-auto px-4 py-8">
      <h1 className="text-2xl font-bold mb-4">Latest Posts</h1>
      <ul>
        {posts.map((post) => (
          <li key={post.id} className="mb-4">
            <Link href={`/posts/${post.id}`}>
              <span className="text-blue-500 hover:underline">{post.title}</span>
            </Link>
          </li>
        ))}
      </ul>
      <div className="flex justify-center mt-8">
        {pages.map((page) => (
          <button
            key={page}
            className={`px-3 py-1 mr-2 rounded ${
              page === currentPage ? "bg-blue-500 text-white" : "bg-gray-200"
            }`}
            onClick={() => handlePageChange(page)}
          >
            {page}
          </button>
        ))}
      </div>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async ({
  query,
}) => {
  const page = query.page ? parseInt(query.page.toString(), 10) : 1;

  const response = await axios.get(
    `https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=${PAGE_SIZE}`
  );

  const totalPosts = parseInt(response.headers["x-total-count"], 10);
  const posts = response.data as Post[];

  return {
    props: {
      posts,
      totalPosts,
    },
  };
};